﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[,] initials = new string[0, 3];
            string[] duty = new string[0];
            bool isSearch = true;

            Menu(initials, duty, isSearch);

           
        }
        
        static void Menu(string[,] initials, string[] duty, bool isSearch)
        {
            do
            {
                Console.WriteLine("\n1 - добавить досье.\n2 - вывести все досье.\n3 - удалить досье.\n4 - поиск по фамилии.\n5 - выход.");

                ConsoleKeyInfo charKey = Console.ReadKey();
                switch (charKey.Key)
                {
                    case ConsoleKey.D1:
                        ExpansionInitials(ref initials);

                        ExpansionDuty(ref duty);

                        Console.ReadKey();
                        break;
                    case ConsoleKey.D2:
                        Console.WriteLine("\n" + ViewAllFiles(initials, duty));

                        Console.ReadKey();
                        break;
                    case ConsoleKey.D3:
                        int id = SearchForDossier(initials);

                        DeleteDossier(ref initials, id);    
                        DeleteDossier(ref duty, id);
                        break;
                    case ConsoleKey.D4:
                        int index = SearchForDossier(initials);

                        Console.WriteLine(initials[index, 0] + " " + initials[index, 1] + " " + initials[index, 2] + " - " + duty[index]);

                        Console.ReadKey();
                        break;
                    case ConsoleKey.D5:
                        Console.WriteLine("\nДо встречи!");

                        isSearch = false;

                        Console.ReadKey();
                        return;
                        
                }

                Console.Clear();
            }
            while (isSearch);
            
        }

        static void ExpansionInitials(ref string[,] initials)
        {
            Console.WriteLine("\nНапишите ваше фамилию:");
            string lastName = Console.ReadLine();

            Console.WriteLine("Напишите ваше имя:");
            string name = Console.ReadLine();

            Console.WriteLine("Напишите ваше отчество:");
            string patronymic = Console.ReadLine();

            initials = Resize(initials, initials.GetLength(0) + 1, initials.GetLength(1));

            initials[initials.GetLength(0) - 1, 0] = lastName;
            initials[initials.GetLength(0) - 1, 1] = name;
            initials[initials.GetLength(0) - 1, 2] = patronymic;
        }

        static void ExpansionDuty(ref string[] array)
        {
            Console.WriteLine("\nВведите вашу должность:");
            string duty = Console.ReadLine();
            
            array = Resize(array, array.Length + 1);

            array[array.Length - 1] = duty;
        }

        static string[] Resize(string[] array, int size)
        {
            string[] tempArray = new string[size];

            for (int i = 0; i < array.Length; i++)
            {
                tempArray[i] = array[i];
            }

            array = tempArray;

            return array;
        }

        static string[,] Resize(string[,] array, int sizeOne, int sizeTwo)
        {
            string[,] tempArray = new string[sizeOne, sizeTwo];

            for(int i = 0; i < array.GetLength(0); i++)
            {
                for(int j = 0; j < array.GetLength(1); j++)
                {
                    tempArray[i, j] = array[i, j];
                }
            }

            array = tempArray;

            return array;
        }

        static string ViewAllFiles(string[,] initials, string[] duty)
        {
            string str = "";

            for (int i = 0; i < initials.GetLength(0); i++)
            {
                

                for (int j = 0; j < initials.GetLength(1); j++)
                {
                    str += initials[i, j] + " ";
                }

                str += "- " + duty[i] + ".\n";
            }

            return str;
        }

        static int SearchForDossier(string[,] array)
        {
            Console.WriteLine("\nВведите фамилию:");
            string lastName = Console.ReadLine();

            for(int i = 0; i < array.GetLength(0); i++)
            {
                if(lastName == array[i,0])
                {
                    return i;                    
                }
            }

            return 0;
        }

        static void DeleteDossier(ref string[,] array, int index)
        {
            string[,] tempArray = new string[array.GetLength(0) - 1, array.GetLength(1)];

            for (int i = 0; i < index; i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    tempArray[i, j] = array[i, j];
                }
            }

            for (int i = index + 1; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    tempArray[i - 1, j] = array[i, j];
                }
            }

            array = tempArray;
        }

        static void DeleteDossier(ref string[] array, int index)
        {
            string[] tempArray = new string[array.Length - 1];

            for(int i = 0; i < index; i++)
            {
                tempArray[i] = array[i];
            }

            for(int i = index + 1; i < array.Length; i++)
            {
                tempArray[i - 1] = array[i];
            }

            array = tempArray;
        }
    }
}
